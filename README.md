### Hi there, I'm DuongNH - younger developer 👋

# SotaTek Test Project

### 📗 📘 📙 Todo List 📓 📔 📒

### Information:
- Link mockup design: [Mockup](https://wireframepro.mockflow.com/view/Mdab887a4c13f1076aad51db6687a2c8e1593539076138#/page/3d47feb9951d457bbdc7e64b49f9f904)
- Require: [PDF](https://drive.google.com/file/d/1VvoQH7UAgjq-yt1TVAe5t74DNN8saElt/view?usp=sharing)

### Preview 😉:
![2021-03-31_0-08-16](/uploads/53bea02445fe40e2df28a2506f7fbc4c/2021-03-31_0-08-16.png)

### How to run in local:
- Pull or download zip source in branch `duongnh-develop`
- Install yarn if you have not `npm install -g yarn`, you can check version when installed `yarn -v`
- Install node modules `yarn upgrade`
- Then, run `yarn start`
- Check your local web in http://localhost:3000/

### Test website in netlify
**I have deploy the TodoList application on netlify, you can try: https://stoic-yonath-26c139.netlify.app/**
- The application deploy on [Netlify](https://app.netlify.com/)
- Pipe using [Buddy](https://app.buddy.works/)
- Deploy by source in `master` branch
