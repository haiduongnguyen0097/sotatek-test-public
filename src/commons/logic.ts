import moment from "moment";
import { EMPTY_STRING, FORMAT_DATE } from "./constant";
import { TaskType } from "./type";

export const momentToString = (
  date: moment.Moment,
  formatDate = FORMAT_DATE
) => {
  return moment(date).format(formatDate);
};

export const compareDate = (
  dateFirst: moment.Moment,
  dateSecond: moment.Moment
) => {
  if (momentToString(dateFirst) < momentToString(dateSecond)) {
    return -1;
  }
  if (momentToString(dateFirst) > momentToString(dateSecond)) {
    return 1;
  }
  return 0;
};

export const compareTaskByDueDate = (
  taskFirst: TaskType,
  taskSecond: TaskType
): number => {
  return compareDate(taskFirst.dueDate, taskSecond.dueDate);
};

export const handleSearchTodoListByName = (
  storageData: TaskType[],
  searchText = EMPTY_STRING
): TaskType[] => {
  if (!storageData) {
    return [];
  }
  if (searchText === EMPTY_STRING) {
    return storageData;
  } else {
    return storageData.filter((item) => item.name.includes(searchText));
  }
};
