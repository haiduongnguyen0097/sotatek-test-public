import moment from "moment";
import { PRIORITY_LEVEL } from "./constant";

export enum TaskMode {
  CREATE,
  UPDATE,
  DELETE,
}

export type TaskType = {
  taskId: string;
  name: string;
  desciption: string;
  dueDate: moment.Moment;
  piority: typeof PRIORITY_LEVEL.Normal;
  isShowDetail: boolean;
};

export type TodoListType = {
  tasks: TaskType[];
  listChecked: string[];
};

export type OptionSelectType = {
  value: string;
  label: string;
};
