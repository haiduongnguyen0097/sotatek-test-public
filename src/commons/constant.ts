import { OptionSelectType } from "./type";

export const STORAGE_KEY = {
  TODO_LIST_DATA: "todoListStorageData",
};

export enum TaskChangedType {
  NAME,
  DESCIPTION,
  DUEDATE,
  PIORITY,
}

export const FORMAT_DATE = "YYYY-MM-DD";

export const MESSAGE_ERROR = {
  TASK_NAME_IS_EMPTY: "ERR_001",
  DUE_DATE_PAST_DAY: "ERR_002",
};

export const MESSAGE_SUCCESS = {
  CREATE_SUCCESS: "MSG_001",
  UPDATE_SUCCESS: "MSG_002",
  DELETE_SUCCESS: "MSG_003",
  REMOVE_SUCCESS: "MSG_004",
};

export const PRIORITY_LEVEL = {
  Low: "1",
  Normal: "2",
  High: "3",
};

export const PRIORITY_OPTION = [
  { value: PRIORITY_LEVEL.Low, label: "Low" },
  { value: PRIORITY_LEVEL.Normal, label: "Normal" },
  { value: PRIORITY_LEVEL.High, label: "High" },
] as OptionSelectType[];

export const DEFAULT_PRIORITY = PRIORITY_LEVEL.Normal;

export const EMPTY_STRING = "";

export const TYPE_CHECKED = {
  CHECK: true,
  UNCHECK: false,
};
