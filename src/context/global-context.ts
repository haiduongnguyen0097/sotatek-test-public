import { createContext, useContext } from "react";

export type GlobalContent = {
  searchItem: string;
  setSearchItem: React.Dispatch<React.SetStateAction<string>>;
};
export const GlobalContext = createContext<GlobalContent>({} as GlobalContent);
export const useGlobalContext = () => useContext(GlobalContext);
