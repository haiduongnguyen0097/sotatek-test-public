import React from "react";
import ReactDOM from "react-dom";
import TodoApp from "./todo-app";
import "./style/index.css";
import "./i18n";

ReactDOM.render(
  <React.StrictMode>
    <TodoApp />
  </React.StrictMode>,
  document.getElementById("root")
);
