import { useEffect, useState } from "react";
import CreateEditTask from "./components/create-edit-task";
import update from "immutability-helper";
import TaskList from "./components/task-list";
import { v4 as uuidv4 } from "uuid";
import { useForceRender } from "./helpers/use-force-render";
import moment from "moment";
import { TaskMode, TodoListType, TaskType } from "./commons/type";
import {
  DEFAULT_PRIORITY,
  EMPTY_STRING,
  MESSAGE_ERROR,
  MESSAGE_SUCCESS,
  STORAGE_KEY,
  TYPE_CHECKED,
} from "./commons/constant";
import {
  compareDate,
  compareTaskByDueDate,
  handleSearchTodoListByName,
} from "./commons/logic";
import { useLocalStorage } from "./helpers/use-local-storage";
import BulkTask from "./components/bulk-task";
import { GlobalContext } from "./context/global-context";
import { useDebounce } from "use-debounce/lib";
import { useTranslation } from "react-i18next";

const DEFAULT_TODO_LIST_DATA: TodoListType = {
  tasks: [
    {
      taskId: "1",
      name: "Task name 1",
      desciption: "Task description 1",
      dueDate: moment(),
      piority: DEFAULT_PRIORITY,
      isShowDetail: true,
    },
  ],
  listChecked: ["1"],
};

const TodoApp = (): JSX.Element => {
  const { t } = useTranslation();

  const [
    todoListStorageData,
    setTodoListStorageData,
  ] = useLocalStorage<TodoListType>(
    STORAGE_KEY.TODO_LIST_DATA,
    DEFAULT_TODO_LIST_DATA
  );

  const [searchItem, setSearchItem] = useState(EMPTY_STRING);
  const debouncedSearchItem = useDebounce(searchItem, 500)[0];
  const [todoListFilter, setTodoListFilter] = useState<TaskType[]>(() =>
    handleSearchTodoListByName(todoListStorageData.tasks)
  );
  const [listChecked, setListChecked] = useState<string[]>(
    todoListStorageData.listChecked ? todoListStorageData.listChecked : []
  );

  const [isRenderCreateTask, forceRenderCreateTask] = useForceRender();

  const showAlert = (message: string): string => {
    alert(message);
    return message;
  };

  const validateTask = (mode: TaskMode, taskData: TaskType): string | null => {
    if (mode === TaskMode.DELETE) {
      return null;
    }

    // TaskMode.CREATE or TaskMode.UPDATE
    if (taskData.name === EMPTY_STRING) {
      return showAlert(t(`error.${MESSAGE_ERROR.TASK_NAME_IS_EMPTY}`));
    } else if (compareDate(taskData.dueDate, moment()) < 0) {
      return showAlert(t(`error.${MESSAGE_ERROR.DUE_DATE_PAST_DAY}`));
    }
    return null;
  };

  const handleUpdateListChecked = (taskId: string, typeChecked: boolean) => {
    if (typeChecked === TYPE_CHECKED.CHECK) {
      setListChecked(
        Array.from(new Set(update(listChecked, { $push: [taskId] })))
      );
    } else {
      // typeChecked === TYPE_CHECKED.UNCHECK
      setListChecked(listChecked.filter((item) => item !== taskId));
    }
  };

  const handleActionTaskData = (mode: TaskMode, taskData: TaskType) => {
    if (validateTask(mode, taskData) !== null) {
      return;
    }

    if (mode === TaskMode.CREATE) {
      const taskDataWithId = update(taskData, { taskId: { $set: uuidv4() } });
      const newStorageTaskData = update(todoListStorageData, {
        tasks: { $push: [taskDataWithId] },
      });
      newStorageTaskData.tasks.sort(compareTaskByDueDate);
      setTodoListStorageData(newStorageTaskData);
      forceRenderCreateTask();
      showAlert(
        t(`message.${MESSAGE_SUCCESS.CREATE_SUCCESS}`, {
          taskName: taskData.name,
        })
      );
    } else if (mode === TaskMode.UPDATE) {
      const indexUpdate = todoListStorageData.tasks.findIndex(
        (task) => task.taskId === taskData.taskId
      );
      const newStorageTaskData = update(todoListStorageData, {
        tasks: { [indexUpdate]: { $set: taskData } },
      });
      newStorageTaskData.tasks.sort(compareTaskByDueDate);
      setTodoListStorageData(newStorageTaskData);
      showAlert(
        t(`message.${MESSAGE_SUCCESS.UPDATE_SUCCESS}`, {
          taskName: taskData.name,
        })
      );
    } else {
      // mode === TaskMode.DELETE
      const newTasks = todoListStorageData.tasks.filter(
        (task) => task.taskId !== taskData.taskId
      );
      const newListChecked = todoListStorageData.listChecked.filter(
        (item) => item !== taskData.taskId
      );
      const newStorageTaskData = update(todoListStorageData, {
        tasks: { $set: newTasks },
        listChecked: { $set: newListChecked },
      });
      setTodoListStorageData(newStorageTaskData);
      setListChecked(newListChecked);
      showAlert(
        t(`message.${MESSAGE_SUCCESS.DELETE_SUCCESS}`, {
          taskName: taskData.name,
        })
      );
    }
    // Reset value searchtext when update listTask
    setSearchItem(EMPTY_STRING);
  };

  const handleRemoveMultipleTask = () => {
    if (listChecked) {
      const newTasks = todoListStorageData.tasks.filter(
        (task) => !listChecked.includes(task.taskId)
      );
      const newStorageTaskData = update(todoListStorageData, {
        tasks: { $set: newTasks },
        listChecked: { $set: [] },
      });
      setTodoListStorageData(newStorageTaskData);
      setListChecked([]);

      // Reset value searchtext when update listTask
      setSearchItem(EMPTY_STRING);
    }
  };

  useEffect(() => {
    const newTodoListFilter = handleSearchTodoListByName(
      todoListStorageData.tasks,
      debouncedSearchItem
    );
    setTodoListFilter(newTodoListFilter);
  }, [debouncedSearchItem, todoListStorageData.tasks]);

  useEffect(() => {
    if (listChecked) {
      setTodoListStorageData(
        update(todoListStorageData, { listChecked: { $set: listChecked } })
      );
    }
  }, [listChecked, setTodoListStorageData, todoListStorageData]);

  return (
    <GlobalContext.Provider value={{ searchItem, setSearchItem }}>
      <div className="app">
        <div className="left-content">
          {isRenderCreateTask && (
            <CreateEditTask
              mode={TaskMode.CREATE}
              handleActionTaskData={handleActionTaskData}
            ></CreateEditTask>
          )}
        </div>
        <div className="right-content">
          <div className="main-content">
            <TaskList
              tasks={todoListFilter}
              handleActionTaskData={handleActionTaskData}
              handleUpdateListChecked={handleUpdateListChecked}
              listChecked={listChecked}
            ></TaskList>
          </div>
          {listChecked.length > 0 && (
            <BulkTask
              handleRemoveMultipleTask={handleRemoveMultipleTask}
            ></BulkTask>
          )}
        </div>
      </div>
    </GlobalContext.Provider>
  );
};

export default TodoApp;
