import { FC, useState } from "react";
import update from "immutability-helper";
import moment from "moment";
import { TaskMode, TaskType } from "../commons/type";
import {
  EMPTY_STRING,
  FORMAT_DATE,
  PRIORITY_LEVEL,
  PRIORITY_OPTION,
  TaskChangedType,
} from "../commons/constant";
import { momentToString } from "../commons/logic";
import { useTranslation } from "react-i18next";

type GeneralTaskProps = {
  handleActionTaskData: (mode: TaskMode, taskData: TaskType) => void;
};

type CreateTaskProps = {
  mode: TaskMode.CREATE;
} & GeneralTaskProps;

type EditTaskProps = {
  mode: TaskMode.UPDATE;
  taskData: TaskType;
} & GeneralTaskProps;

type GenericCreateEditTaskProps<T> = T extends TaskMode.CREATE
  ? CreateTaskProps
  : EditTaskProps;

type CreateEditTaskProps<T = any> = FC<GenericCreateEditTaskProps<T>>;

const DEFAULT_TASK_DATA: TaskType = {
  taskId: "0", // render random id when submit
  name: EMPTY_STRING,
  desciption: EMPTY_STRING,
  dueDate: moment(),
  piority: PRIORITY_LEVEL.Normal,
  isShowDetail: false, // default set false
};

const CreateEditTask: CreateEditTaskProps = (props): JSX.Element => {
  const { t } = useTranslation();

  const [taskData, setTaskData] = useState<TaskType>(
    props.mode === TaskMode.UPDATE ? props.taskData : DEFAULT_TASK_DATA
  );

  const handleTaskDataChanged = (type: TaskChangedType, value: string) => {
    switch (type) {
      case TaskChangedType.NAME: {
        const newTaskData = update(taskData, {
          name: { $set: value },
        });
        setTaskData(newTaskData);
        break;
      }
      case TaskChangedType.DESCIPTION: {
        const newTaskData = update(taskData, {
          desciption: { $set: value },
        });
        setTaskData(newTaskData);
        break;
      }
      case TaskChangedType.DUEDATE: {
        const newTaskData = update(taskData, {
          dueDate: {
            $set: moment(value, FORMAT_DATE),
          },
        });
        setTaskData(newTaskData);
        break;
      }
      case TaskChangedType.PIORITY: {
        const newTaskData = update(taskData, {
          piority: {
            $set: value,
          },
        });
        setTaskData(newTaskData);
        break;
      }
      default:
        break;
    }
  };

  const handleActionTaskData = () => {
    props.handleActionTaskData(props.mode, taskData);
  };

  return (
    <>
      {props.mode === TaskMode.CREATE && (
        <h2 className="text-align-center">
          {t("todo_app.create_edit_task.title_create_task")}
        </h2>
      )}
      <div
        className={`task-form ${
          props.mode === TaskMode.CREATE ? "create" : "update"
        }`}
      >
        <div className="field-mt-12">
          <input
            type="text"
            className="text-input"
            placeholder={t("todo_app.create_edit_task.task_name_placeholder")}
            value={taskData.name}
            onChange={(event) =>
              handleTaskDataChanged(TaskChangedType.NAME, event.target.value)
            }
          />
        </div>
        <div className="field-mt-12">
          <label className="field-name">
            {t("todo_app.create_edit_task.description")}
          </label>
          <textarea
            className="text-area"
            value={taskData.desciption}
            onChange={(event) =>
              handleTaskDataChanged(
                TaskChangedType.DESCIPTION,
                event.target.value
              )
            }
          />
        </div>
        <div className="field-mt-6 left">
          <label className="field-name">
            {t("todo_app.create_edit_task.due_date")}
          </label>
          <input
            type="date"
            className="date-input"
            value={momentToString(taskData.dueDate)}
            onChange={(event) =>
              handleTaskDataChanged(TaskChangedType.DUEDATE, event.target.value)
            }
          />
        </div>
        <div className="field-mt-6 right">
          <label className="field-name">
            {t("todo_app.create_edit_task.piority")}
          </label>
          <select
            className="drop-down"
            value={taskData.piority}
            onChange={(event) =>
              handleTaskDataChanged(TaskChangedType.PIORITY, event.target.value)
            }
          >
            {PRIORITY_OPTION.map((item) => {
              return (
                <option key={item.value} value={item.value}>
                  {item.label}
                </option>
              );
            })}
          </select>
        </div>
        <button className="button green" onClick={() => handleActionTaskData()}>
          {props.mode === TaskMode.CREATE
            ? t("todo_app.create_edit_task.btn_add")
            : t("todo_app.create_edit_task.btn_update")}
        </button>
      </div>
    </>
  );
};

export default CreateEditTask;
