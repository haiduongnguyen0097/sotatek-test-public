import { FC } from "react";
import { useTranslation } from "react-i18next";

type BulkTaskProps = {
  handleRemoveMultipleTask: () => void;
};

const BulkTask: FC<BulkTaskProps> = (props): JSX.Element => {
  const { t } = useTranslation();

  return (
    <div className="bottom-content">
      <div className="field-mt-12 field-button">
        <div className="bottom-label">
          <label>{t("todo_app.bulk_task.bulk_action")}</label>
        </div>
        <div className="bottom-action">
          <button className="button done-all">
            {t("todo_app.bulk_task.btn_done")}
          </button>
          <button
            className="button remove-all"
            onClick={() => props.handleRemoveMultipleTask()}
          >
            {t("todo_app.bulk_task.btn_remove")}
          </button>
        </div>
      </div>
    </div>
  );
};

export default BulkTask;
