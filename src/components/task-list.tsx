import { FC } from "react";
import { useTranslation } from "react-i18next";
import { TaskMode, TaskType } from "../commons/type";
import { useGlobalContext } from "../context/global-context";
import TaskItem from "./task-item";

type TaskListProps = {
  tasks: TaskType[];
  handleActionTaskData: (mode: TaskMode, taskData: TaskType) => void;
  listChecked: string[];
  handleUpdateListChecked: (taskId: string, typeChecked: boolean) => void;
};

const TaskList: FC<TaskListProps> = (props): JSX.Element => {
  const { t } = useTranslation();

  const { tasks, listChecked } = props;
  const { searchItem, setSearchItem } = useGlobalContext();

  return (
    <>
      <h2 className="text-align-center">
        {t("todo_app.task_list.title_todo_list")}
      </h2>
      <input
        type="text"
        className="text-input"
        placeholder={t("todo_app.task_list.text_search_placeholder")}
        value={searchItem}
        onChange={(event) => {
          setSearchItem(event.target.value);
        }}
      />
      <div
        className={`todo-list ${
          listChecked && listChecked.length > 0 ? "" : "hide-bottom"
        }`}
      >
        {tasks.map((task) => {
          return (
            <TaskItem
              key={task.taskId}
              task={task}
              isShowDetail={task.isShowDetail}
              isChecked={listChecked && listChecked.includes(task.taskId)}
              handleActionTaskData={props.handleActionTaskData}
              handleUpdateListChecked={props.handleUpdateListChecked}
            ></TaskItem>
          );
        })}
      </div>
    </>
  );
};

TaskList.defaultProps = {
  tasks: [],
};

export default TaskList;
