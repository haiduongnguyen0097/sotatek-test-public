import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { TaskMode, TaskType } from "../commons/type";
import CreateEditTask from "./create-edit-task";

type TaskItemProps = {
  task: TaskType;
  isShowDetail: boolean;
  isChecked: boolean;
  handleActionTaskData: (mode: TaskMode, taskData: TaskType) => void;
  handleUpdateListChecked: (taskId: string, typeChecked: boolean) => void;
};

const TaskItem: FC<TaskItemProps> = (props): JSX.Element => {
  const { t } = useTranslation();

  const { task, isChecked } = props;
  const [isShowDetail, setIsShowDetail] = useState(props.isShowDetail);

  return (
    <div className="todo-item" key={task.taskId}>
      <div className="todo-view">
        <div className="todo-info">
          <label className="text">
            <input
              className="check-box"
              type="checkbox"
              checked={isChecked}
              onChange={(event) =>
                props.handleUpdateListChecked(task.taskId, event.target.checked)
              }
            />
            <span>{task.name}</span>
          </label>
        </div>
        <div className="todo-action">
          <button
            className="button detail-todo"
            onClick={() => setIsShowDetail(!isShowDetail)}
          >
            {t("todo_app.task_item.btn_detail")}
          </button>
          <button
            className="button remove-todo"
            onClick={() => props.handleActionTaskData(TaskMode.DELETE, task)}
          >
            {t("todo_app.task_item.btn_remove")}
          </button>
        </div>
      </div>
      {isShowDetail && (
        <CreateEditTask
          mode={TaskMode.UPDATE}
          taskData={task}
          handleActionTaskData={props.handleActionTaskData}
        ></CreateEditTask>
      )}
    </div>
  );
};

TaskItem.defaultProps = {
  isShowDetail: false,
  isChecked: false,
};

export default TaskItem;
