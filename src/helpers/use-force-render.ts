import { useEffect, useState } from "react";

export const useForceRender = (): [boolean, () => void] => {
  const [isRender, setIsRender] = useState<boolean>(true);

  useEffect(() => {
    !isRender && setIsRender(true);
    return () => {};
  }, [isRender]);

  const forceRender = (): void => {
    setIsRender(false);
  };

  return [isRender, forceRender];
};
